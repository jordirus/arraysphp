<?php
$matriu = generarMatriu(4);
mostrarMatriu($matriu);
mostrarMatriu(girarMatriu($matriu));
function generarMatriu($valor){
	$Array = array(array());
	for($x = 0; $x < $valor; $x++){
		for($y = 0; $y < $valor; $y++) {
			if ($y === $x) {
				$Array[$x][$y] = "*";
			} else {
				if ($y > $x) {
					$Array[$x][$y] = $y + $x;
				} else {
					$Array[$x][$y] = rand(10,20);
				}
			}
		}
	}
	return $Array;
}
function mostrarMatriu($matriu){
	$valor = count($matriu[0]);
	print_r("<center>");
	print_r("<table>");
	for($x = 0; $x < $valor; $x++) {
		print_r("<tr>");
		for($y = 0; $y < $valor; $y++) {
			print_r("<td style= \"border-style:solid\">" . $matriu[$x][$y] . "</td>");
		}
		print_r("<br>");
	}
	print_r("</table>");
	print_r("</center>");
}
function girarMatriu($matriu){
	$Array = array(array());
	$valor = count($matriu[0]);
	for($x = 0; $x < $valor; $x++){
		for($y = 0; $y < $valor; $y++) {
			$girarMatriu[$x][$y] = $matriu[$y][$x];
			$girarMatriu[$y][$x] = $matriu[$x][$y];
		}
	}
	return $girarMatriu;
}
?>